import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static void initialiserUtilisateur(HashMap<String, String> utilisateur) {
        utilisateur.put("id", "0");
        utilisateur.put("nom", "");
        utilisateur.put("email", "");
        utilisateur.put("age", "");
    }

    public static void afficherMenu(String[] menu) {
        for (String menuItem : menu) {
            System.out.println(menuItem);
        }
        System.out.print("Votre choix : ");
    }

    public static int demanderChoix() {
        Scanner scanner = new Scanner(System.in);
        int choice;
        try {
            choice = scanner.nextInt();
        } catch (Exception e) {
            choice = -1;
        }
        scanner.nextLine();
        return choice;
    }

    public static void creerCompte(ArrayList<HashMap<String, String>> utilisateurs) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Merci de rentrer un nom d'utilisateur pour créer votre compte");
        HashMap<String, String> nouvelUtilisateur = new HashMap<>();
        nouvelUtilisateur.put("nom", scanner.nextLine());
        nouvelUtilisateur.put("id", ((Integer) (utilisateurs.size() + 1)).toString());

        boolean success = utilisateurs.add(nouvelUtilisateur);
        if (!success) {
            System.out.println("Ce compte existe déjà...");
            System.out.println();
        } else {
            System.out.println("Compte créé avec succès !");
            System.out.println();
        }
    }

    public static void afficherComptes(ArrayList<HashMap<String, String>> utilisateurs) {
        if (utilisateurs.size() == 0) {
            System.out.println("Il n'y a pas de compte actuellement...");
            System.out.println();
        } else {
            System.out.println("Voici la liste des comptes :");
            for (HashMap<String, String> account : utilisateurs) {
                System.out.println(
                        "n°" + account.get("id") + " -> " + account.get("nom") +
                                (account.get("email") != null ? ", " + account.get("email") : "") +
                                (account.get("age") != null ? ", " + account.get("age") + " ans" : ""));
            }
            System.out.println();
        }
    }

    public static void connexion(ArrayList<HashMap<String, String>> utilisateurs, HashMap<String, String> utilisateur) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Merci de rentrer un nom d'utilisateur pour vous connecter");
        String nom = scanner.nextLine();
        for (HashMap<String, String> account : utilisateurs) {
            if (Objects.equals(account.get("nom"), nom)) {
                System.out.println("Connexion réussie ! Bienvenue " + nom + " !");
                System.out.println();
                utilisateur.put("id", account.get("id"));
                utilisateur.put("nom", account.get("nom"));
                utilisateur.put("email", account.get("email"));
                utilisateur.put("age", account.get("age"));
                return;
            }
        }
        System.out.println("Echec de la connexion...");
        System.out.println();
    }

    public static void resetUser(HashMap<String, String> utilisateur) {
        utilisateur.put("id", "0");
        utilisateur.put("nom", "");
        utilisateur.put("email", "");
        utilisateur.put("age", "");
    }

    public static void deconnexion(HashMap<String, String> utilisateur, ArrayList<HashMap<String, String>> utilisateurs) {
        for (HashMap<String, String> account : utilisateurs) {
            if (Objects.equals(account.get("id"), utilisateur.get("id"))) {
                System.out.println("Trouvé");
                account.put("nom", utilisateur.get("nom"));
                account.put("email", utilisateur.get("email"));
                account.put("age", utilisateur.get("age"));
            }
        }
        resetUser(utilisateur);
        System.out.println("Vous vous êtes déconnecté avec succès");
        System.out.println();
    }

    public static void modifierProfil(String[] menuProfil, HashMap<String, String> utilisateur) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(utilisateur.get("nom") + ", que souhaitez-vous modifier dans votre profil ?");
        boolean leaveMenu = false;
        while (!leaveMenu) {
            afficherMenu(menuProfil);
            switch (demanderChoix()) {
                case 1 -> {
                    System.out.print("Modifier votre nom : ");
                    utilisateur.put("nom", scanner.nextLine());
                }
                case 2 -> {
                    System.out.print("Modifier votre email : ");
                    utilisateur.put("email", scanner.nextLine());
                }
                case 3 -> {
                    System.out.print("Modifier votre age : ");
                    utilisateur.put("age", scanner.nextLine());
                }
                case 4 -> {
                    leaveMenu = true;
                }
                default -> defaultSwitch();
            }
        }
    }

    public static void arretProgramme() {
        System.out.println("Arrêt du programme... Veuillez patienter... Bip bip... Piiiouuuu...");
        System.exit(0);
    }

    public static void defaultSwitch() {
        System.out.println("Ce choix ne correspond à rien...");
        System.out.println();
    }

    public static void main(String[] args) {
        final String[] menuDeco = {
                "1 - Connexion",
                "2 - Créer un compte",
                "3 - Afficher les comptes",
                "4 - Quitter le programme"
        };
        final String[] menuCo = {
                "1 - Déconnexion",
                "2 - Modifier mon profil",
                "3 - Quitter le programme"
        };
        final String[] menuProfil = {
                "1 - Nom",
                "2 - Email",
                "3 - Age",
                "4 - Revenir en arrière..."
        };

        HashMap<String, String> utilisateur = new HashMap<>();
        initialiserUtilisateur(utilisateur);

        ArrayList<HashMap<String, String>> utilisateurs = new ArrayList<>();

        System.out.println("Bienvenue sur notre service de gestion de comptes !");
        System.out.println();

        int end = 0;
        boolean connecte;
        while (end < 100) {
            connecte = utilisateur.get("nom").length() != 0;
            System.out.println(connecte ? "  -> Vous êtes connecté : " + utilisateur.get("nom") : "  -> Vous n'êtes pas connecté");

            if (connecte) {
                afficherMenu(menuCo);
                switch (demanderChoix()) {
                    case 1 -> deconnexion(utilisateur, utilisateurs);
                    case 2 -> modifierProfil(menuProfil, utilisateur);
                    case 3 -> arretProgramme();
                    default -> defaultSwitch();
                }
            } else {
                afficherMenu(menuDeco);
                switch (demanderChoix()) {
                    case 1 -> connexion(utilisateurs, utilisateur);
                    case 2 -> creerCompte(utilisateurs);
                    case 3 -> afficherComptes(utilisateurs);
                    case 4 -> arretProgramme();
                    default -> defaultSwitch();
                }
            }
            end++;
        }
    }
}